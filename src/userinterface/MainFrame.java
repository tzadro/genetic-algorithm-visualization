package userinterface;

import java.util.List;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import utility.Function;
import utility.Function2;
import utility.Function3;
import utility.IFunction;
 
public class MainFrame extends Application {
	static int PANE_RESOLUTION = 768;	
	static String POPULATION_SIZE = "10";
	static String GENETIC_MATERIAL_SIZE = "14";
	static String PAUSE_TIME = "100";
	
	SimulationPane simulationPane;
	VBox labelsPane;
 
    @Override
    public void start(Stage mainStage) {
    	// init
    	mainStage.setTitle("Genetic Algorithm Visualization");
        mainStage.setOnCloseRequest(e -> closeProgram());
        
        // layout
        BorderPane layout = new BorderPane();
		
        // center
        simulationPane = new SimulationPane(PANE_RESOLUTION);
		layout.setCenter(simulationPane);
        
        // left
    	labelsPane = new VBox();
    	labelsPane.setAlignment(Pos.TOP_LEFT);
    	labelsPane.setPadding(new Insets(50, 50, 50, 50));
    	initLabelsPane(null);
		layout.setLeft(labelsPane);
		
		// right
		GridPane settingsPane = new GridPane();
		settingsPane.setAlignment(Pos.TOP_CENTER);
		settingsPane.setPadding(new Insets(50, 50, 50, 50));
		settingsPane.setHgap(10);
		settingsPane.setVgap(12);

    	Label rightTitle = new Label("Settings");
    	rightTitle.setFont(new Font("Calibri", 30));
    	rightTitle.setAlignment(Pos.TOP_CENTER);
		settingsPane.add(rightTitle, 0, 0, 2, 1);
		
		Label functionLabel = new Label("Function:");
		ComboBox<IFunction> functionInput = new ComboBox<>();
		IFunction function = new Function();
		functionInput.getItems().add(function);
		functionInput.getItems().add(new Function2());
		functionInput.getItems().add(new Function3());
		functionInput.setValue(function);
		settingsPane.add(functionLabel, 0, 1);
		settingsPane.add(functionInput, 1, 1);
		
		Label populationSizeLabel = new Label("Population size:");
		TextField populationSizeInput = new TextField(POPULATION_SIZE);
		Label geneticMaterialSizeLabel = new Label("Genetic material size:");
		TextField geneticMaterialSizeInput = new TextField(GENETIC_MATERIAL_SIZE);
		Label pauseTimeLabel = new Label("Pause time in milis:");
		TextField pauseTimeInput = new TextField(PAUSE_TIME);
		settingsPane.add(populationSizeLabel, 0, 2);
		settingsPane.add(populationSizeInput, 1, 2);
		settingsPane.add(geneticMaterialSizeLabel, 0, 3);
		settingsPane.add(geneticMaterialSizeInput, 1, 3);
		settingsPane.add(pauseTimeLabel, 0, 4);
		settingsPane.add(pauseTimeInput, 1, 4);
		
		HBox buttons = new HBox();
		buttons.setAlignment(Pos.TOP_CENTER);
		buttons.setSpacing(80.0);
		Button start = new Button("Start");
		Button stop = new Button("Stop");
		stop.setDisable(true);
		buttons.getChildren().addAll(start, stop);
	    settingsPane.add(buttons, 0, 5, 2, 1);
		
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setHalignment(HPos.RIGHT);
		settingsPane.getColumnConstraints().add(column1);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setHalignment(HPos.LEFT);
		settingsPane.getColumnConstraints().add(column2);
		
		layout.setRight(settingsPane);
	    
		// show
        mainStage.setScene(new Scene(layout));
        mainStage.show();
        
        // listeners
        start.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	
		    	start.setDisable(true);
		    	stop.setDisable(false);

		    	functionInput.setDisable(true);
		    	IFunction function = functionInput.getValue();
		    	
		    	populationSizeInput.setDisable(true);
		    	int populationSize = Integer.parseInt(populationSizeInput.getText());
		    	
		    	geneticMaterialSizeInput.setDisable(true);
		    	int geneticMaterialSize = Integer.parseInt(geneticMaterialSizeInput.getText());
		    	
		    	pauseTimeInput.setDisable(true);
		    	int pauseTime = Integer.parseInt(pauseTimeInput.getText());
		    	
		    	startSimulation(function, populationSize, geneticMaterialSize, pauseTime);
		    }
		});
        
        stop.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	
		    	functionInput.setDisable(false);
		    	start.setDisable(false);
		    	stop.setDisable(true);
		    	
		    	populationSizeInput.setDisable(false);		    	
		    	geneticMaterialSizeInput.setDisable(false);   	
		    	pauseTimeInput.setDisable(false);
		    	
		    	stopSimulation();
		    }
		});
    }
    
    private void startSimulation(IFunction function, int populationSize, int geneticMaterialSize, int pauseTime) {
        initLabelsPane(simulationPane.start(function, populationSize, geneticMaterialSize, pauseTime));
    }
    
    private void stopSimulation() {
        simulationPane.stop();
    }
    
    private void initLabelsPane(List<Label> labels) {
    	labelsPane.getChildren().clear();
    	
    	Label leftTitle = new Label("Population                 ");
    	leftTitle.setFont(new Font("Calibri", 30));
		labelsPane.getChildren().add(leftTitle);
		VBox.setMargin(leftTitle, new Insets(0, 0, 10, 0));
		
		if (labels != null) {
			for (Label label : labels) {
		    	label.setFont(new Font("Calibri", 20));
		    	label.setAlignment(Pos.CENTER);
				labelsPane.getChildren().add(label);
			}
		}
    }
    
	private void closeProgram() {
		Runtime.getRuntime().halt(0);
	}
}