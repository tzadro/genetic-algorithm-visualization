package userinterface;

import java.util.List;

import algorithm.Algorithm;
import algorithm.Domain;
import core.IAlgorithm;
import core.IDomain;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import utility.IFunction;

public class SimulationPane extends Pane {
	private Canvas frontCanvas;
	private Canvas backCanvas;
	
	private int resolution;
	
	private IDomain domain;
	private IAlgorithm algorithm;

	public SimulationPane(int resolution) {
		this.resolution = resolution;
		generateCanvas();
	}
	
	private void generateCanvas() {
		frontCanvas = new Canvas(resolution, resolution);
		backCanvas = new Canvas(resolution, resolution);

		this.getChildren().removeAll();
		this.getChildren().add(frontCanvas);
		this.getChildren().add(backCanvas);
		frontCanvas.toFront();
	}
	
	public List<Label> start(IFunction function, int populationSize, int geneticMaterialSize, int pauseTime) {
		int domainSize = (int) Math.pow(2, geneticMaterialSize / 2);
		
		domain = new Domain(function, domainSize, resolution / domainSize);
		algorithm = new Algorithm(frontCanvas.getGraphicsContext2D(), domain, populationSize, geneticMaterialSize, pauseTime);
    	domain.drawDomain(backCanvas.getGraphicsContext2D());
		
		algorithm.start();
        return algorithm.getLabels();
	}
	
	public void stop() {
		algorithm.stop();
	}
}
