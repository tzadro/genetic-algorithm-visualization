package core;

import java.util.List;

import javafx.scene.control.Label;

public interface IAlgorithm extends Runnable {
	public List<Label> getLabels();
	
	public void start();
	
	public void stop();
}
