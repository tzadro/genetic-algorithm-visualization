package core;

import javafx.scene.control.Label;
import utility.Coordinate;

public interface IIndividual extends Comparable<IIndividual> {
	public double getFitness();

	public IGene getGene();

	public Label getLabel();

	public Coordinate getPosition();
	
	public int getOperation();
	
	public void calculateFitness(IDomain domain);

	public void crossover(IIndividual parent1, IIndividual parent2);
	
	public void setParentColor();
	
	public void mutate();
	
	public void resetColor();
}
