package core;

import javafx.scene.canvas.GraphicsContext;
import utility.Coordinate;

public interface IDomain {
	public double getValueAt(Coordinate c);

	public void drawPopulation(GraphicsContext gc, IPopulation p);
	
	public void drawDomain(GraphicsContext gc);
}
