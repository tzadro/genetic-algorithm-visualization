package core;

import java.util.List;

import javafx.scene.control.Label;

public interface IPopulation {
	public IIndividual getIndividual(int index);
	
	public List<IIndividual> getIndividuals();
	
	public List<Label> getLabels();
	
	public void resetColors();
	
	public List<IIndividual> getRandomIndividuals(int n);
	
	public void caluclatePopulationFitness(IDomain domain);
}
