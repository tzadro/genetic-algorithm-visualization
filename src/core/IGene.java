package core;

import utility.Coordinate;

public interface IGene {
	public Coordinate getPosition();
	
	public String getGeneticMaterial();
	
	public void setRandom();
	
	public void mutate();
	
	public void crossover(IGene parent1, IGene parent2);
}
