package algorithm;

import java.text.DecimalFormat;

import core.IDomain;
import core.IGene;
import core.IIndividual;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import utility.Coordinate;

public class Individual implements IIndividual {
	private static DecimalFormat df2 = new DecimalFormat(".##");
	
	private IGene gene;
	private Label label;
	private int operation;
	private double fitness;

	public Individual(int geneticMaterialSize) {
		gene = new Gene(geneticMaterialSize);
		gene.setRandom();
		label = new Label(gene.getGeneticMaterial());
		operation = 0;
	}

	@Override
	public double getFitness() {
		return fitness;
	}

	@Override
	public IGene getGene() {
		return gene;
	}

	@Override
	public Label getLabel() {
		return label;
	}

	@Override
	public Coordinate getPosition() {
		return gene.getPosition();
	}
	
	@Override
	public int getOperation() {
		int temp = operation;
		operation = 0;
		return temp;
	}

	@Override
	public void calculateFitness(IDomain domain) {
		fitness = domain.getValueAt(gene.getPosition());
		updateLabel("#000000");
	}

	@Override
	public void crossover(IIndividual parent1, IIndividual parent2) {
		gene.crossover(parent1.getGene(), parent2.getGene());
		parent1.setParentColor();
		parent2.setParentColor();
		
		operation = 1;
		updateLabel("#ff0000");
	}
	
	@Override
	public void setParentColor() {
		operation = 2;
		updateLabel("#00ff00");
	}

	@Override
	public void mutate() {
		gene.mutate();
		
		operation = 3;
		updateLabel("#0000ff");
	}
	
	@Override
	public void resetColor() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				updateLabel("#000000");
			}
		});
	}
	
	private void updateLabel(String color) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				label.setText(gene.getGeneticMaterial() + " : " + df2.format(fitness));
				label.setTextFill(Color.web(color));
			}
		});
	}

	@Override
	public int compareTo(IIndividual arg0) {
		if (fitness < arg0.getFitness())
			return 1;
		if (fitness == arg0.getFitness())
			return 0;
		else
			return -1;
	}
}
