package algorithm;

import java.util.Collections;
import java.util.List;

import core.IAlgorithm;
import core.IDomain;
import core.IIndividual;
import core.IPopulation;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;

public class Algorithm implements IAlgorithm, Runnable {
	private GraphicsContext gc;

	private IDomain domain;
	private IPopulation population;
	private int pauseTime;
	private boolean stop = false;
	
	public Algorithm(GraphicsContext gc, IDomain domain, int populationSize, int geneticMaterialSize, int pauseTime) {
		this.gc = gc;
		
		this.domain = domain;
		population = new Population(populationSize, geneticMaterialSize);
		this.pauseTime = pauseTime;
	}

	public List<Label> getLabels() {
		return population.getLabels();
	}

	@Override
	public void start() {
		Thread logic = new Thread(this);
		logic.start();
	}

	@Override
	public void stop() {
		stop = true;
	}

	@Override
	public void run() {
		while (!stop) {
			// draw
			population.resetColors();
			domain.drawPopulation(gc, population);
			pause();
			
			// calculate fitness
			population.caluclatePopulationFitness(domain);
			
			// tournament selection
			List<IIndividual> randomIndividuals = population.getRandomIndividuals(3);
			Collections.sort(randomIndividuals);

			IIndividual individual1 = randomIndividuals.get(0);
			IIndividual individual2 = randomIndividuals.get(1);
			IIndividual individual3 = randomIndividuals.get(2);
			individual3.crossover(individual1, individual2);
			pause();
			
			// mutation
			for (IIndividual i : population.getRandomIndividuals(1))
				i.mutate();
			pause();
		}
		stop = false;
	}
	
	private void pause() {
		try {
			Thread.sleep(pauseTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
