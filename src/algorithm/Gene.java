package algorithm;

import java.util.Random;

import core.IGene;
import utility.Coordinate;

public class Gene implements IGene {
	private String geneticMaterial;
	private int bits;

	public Gene(int bits) {
		this.bits = bits;
	}
	
	@Override
	public Coordinate getPosition() {
		String xbin = (String) geneticMaterial.subSequence(0, bits / 2);
		String ybin = (String) geneticMaterial.subSequence(bits / 2, bits);
		int x = Integer.parseInt(xbin, 2);
		int y = Integer.parseInt(ybin, 2);
		return new Coordinate(x, y);
	}
	
	@Override
	public String getGeneticMaterial() {
		return geneticMaterial;
	}
	
	@Override
	public void setRandom() {
		Random r = new Random();
		
		geneticMaterial = "";
		for (int i = 0; i < bits; i++) {
			geneticMaterial += (Math.abs(r.nextInt()) % 2 == 1) ? "1" : "0";
		}
	}
	

	@Override
	public void mutate() {
		Random r = new Random();
		int l = geneticMaterial.length();
		String newGeneticMaterial = "";
		
		for (int i = 0; i < l; i++) {
			int leftover = r.nextInt() % l;
			String currentBit = "" + geneticMaterial.charAt(i);
			String oppositeBit = currentBit.equals("1") ? "0" : "1";

			if (leftover == 0)
				newGeneticMaterial += oppositeBit;
			else
				newGeneticMaterial += currentBit;
		}
		geneticMaterial = newGeneticMaterial;
	}

	@Override
	public void crossover(IGene parent1, IGene parent2) {
		Random r = new Random();
		String g1 = parent1.getGeneticMaterial();
		String g2 = parent2.getGeneticMaterial();
		
		geneticMaterial = "";
		for (int i = 0; i < bits; i++) {
			geneticMaterial += (Math.abs(r.nextInt()) % 2 == 1) ? g1.charAt(i) : g2.charAt(i);
		}
	}
	
}
