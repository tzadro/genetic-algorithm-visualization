package algorithm;

import core.IDomain;
import core.IIndividual;
import core.IPopulation;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import utility.Coordinate;
import utility.IFunction;

public class Domain implements IDomain {
	private IFunction function;
	int domainSize;
	int blockSize;
	double min;
	double max;
	
	public Domain(IFunction function, int domainSize, int blockSize) {
		this.function = function;
		this.domainSize = domainSize;
		this.blockSize = blockSize;
	}

	@Override
	public double getValueAt(Coordinate c) {
		return function.getValue(c.x, c.y);
	}
	
	@Override
	public void drawPopulation(GraphicsContext gc, IPopulation p) {
		gc.clearRect(0, 0, domainSize * blockSize, domainSize * blockSize);
		
		for (IIndividual i : p.getIndividuals()) {
			Coordinate c = i.getPosition();
			
			switch (i.getOperation()) {
				case 0:
					gc.setFill(Color.PALEVIOLETRED);
					break;
				case 1:
					gc.setFill(Color.RED);
					break;
				case 2:
					gc.setFill(Color.PALEVIOLETRED);
					break;
				case 3:
					gc.setFill(Color.BLUE);

			}
			gc.fillOval(c.y * blockSize, c.x * blockSize, blockSize, blockSize);
		}
	}

	@Override
	public void drawDomain(GraphicsContext gc) {
		double z = function.getValue(0, 0);
		max = z; min = z;
		
		for (int i = 1; i < domainSize; i++) {
			for (int j = 1; j < domainSize; j++) {
				z = function.getValue(i, j);
				
				if (z > max) max = z;
				if (z < min) min = z;
			}
		}
		
		for (int i = 0; i < domainSize; i++) {
			for (int j = 0; j < domainSize; j++) {
				int value = Math.abs((int) Math.round((function.getValue(i, j) - min) / (max - min) * 255));
				Color c = Color.rgb(value, value, value);
				gc.setFill(c);
				gc.fillRect(j * blockSize, i * blockSize, blockSize, blockSize);
			}
		}
	}
}
