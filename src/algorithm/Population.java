package algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import core.IDomain;
import core.IIndividual;
import core.IPopulation;
import javafx.scene.control.Label;

public class Population implements IPopulation {
	private List<IIndividual> individuals;
	private int size;
	
	public Population(int size, int geneticMaterialSize) {
		individuals = new ArrayList<IIndividual>();
		this.size = size;
		
		for (int i = 0; i < size; i++)
			individuals.add(new Individual(geneticMaterialSize));
	}
	
	@Override
	public IIndividual getIndividual(int index) {
		return individuals.get(index);
	}
	
	@Override
	public List<IIndividual> getIndividuals() {
		return individuals;
	}

	@Override
	public List<Label> getLabels() {
		List<Label> labels = new ArrayList<Label>();
		for (IIndividual individual : individuals)
			labels.add(individual.getLabel());
		return labels;
	}

	@Override
	public void resetColors() {
		for (IIndividual i : individuals)
			i.resetColor();
	}

	@Override
	public List<IIndividual> getRandomIndividuals(int n) {
		Random r = new Random();
		List<IIndividual> res = new ArrayList<IIndividual>();
		Set<Integer> used = new HashSet<Integer>();
		
		List<IIndividual> help = individuals;
		Collections.sort(help);
		IIndividual best = help.get(0);
		
		boolean flag = false;
		while (used.size() < n) {
			int index = Math.abs(r.nextInt()) % size;
			
			if (used.contains(index))
				continue;

			if (n == 1 && individuals.get(index) == best)
				continue;
			
			for (int i : used)
				if (individuals.get(i).getFitness() == individuals.get(index).getFitness())
					flag = true;
			if (flag) {
				flag = false;
				continue;
			}
			
			used.add(index);
			res.add(individuals.get(index));
		}
		return res;
	}

	@Override
	public void caluclatePopulationFitness(IDomain domain) {
		double max = 0;
		
		for (IIndividual i : individuals) {
			i.calculateFitness(domain);
			
			if (i.getFitness() > max)
				max = i.getFitness();
		}
	}
}
