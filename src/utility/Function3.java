package utility;

public class Function3 implements IFunction {

	@Override
	public double getValue(int intx, int inty) {
		double x = intx * 16. / 768 - 8;
		double y = inty * 16. / 768 - 8;
		return (x * x + y - 11) * (x * x + y - 11) + (x + y * y - 7) * (x + y * y - 7);
	}
	
	@Override
	public String toString() {
		return "(x * x + y - 11) * (x * x + y - 11) + (x + y * y - 7) * (x + y * y - 7)";
	}
}
