package utility;

public class Function2 implements IFunction {

	@Override
	public double getValue(int intx, int inty) {
		double x = intx * 1.;
		double y = inty * 1.;
		return Math.sin(x / 15.) - Math.cos(y / 15.);
	}
	
	@Override
	public String toString() {
		return "sin(x / 15) - cos(y / 15)";
	}
}
