package utility;

public class Function implements IFunction {

	@Override
	public double getValue(int x, int y) {
		return 400 - Math.sqrt((x - 75) * (x - 75) + (y - 75) * (y - 75));
	}
	
	@Override
	public String toString() {
		return "400 - Math.sqrt((x - 75) * (x - 75) + (y - 75) * (y - 75))";
	}
}
